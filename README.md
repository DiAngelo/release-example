# Release Example
This is a project to test releasing a maven project while trying to conform
to the [GitLab Flow best practices](https://about.gitlab.com/blog/2016/07/27/the-11-rules-of-gitlab-flow/)
## Challange
One of the challenge with GitLab Flow is this rule:
>  6. Tags are set by the user, not by CI.

Problem is that maven release process generates a non snapshot release with an auto generated tag.

## Solution
The solution is to parse `CI_COMMIT_TAG` and extract the desired release, milestone, or release candidate term from the user generated tag.  This value is passed to goal `release:prepare` 
along with the development version.
## Development Version Rules
The development version is determined by the following rules:
* tag ends with `.RELEASE`: this is a release, and patch version is incremented by 1, i.e. `XX.XX.XX+1-SNAPSHOT`
* tag ends with `.M[0-9]+` : this is a milestone, patch version remains same,  i.e. `XX.XX.XX-SNAPSHOT`
* tag ends with `.RC[0-9]+`: this is a release candidate, patch version remains same,  i.e. `XX.XX.XX-SNAPSHOT`

### Tag move
There is no way to stop goal `release:prepare` from creating a release tag containing the released non SNAPSHOT poms.  We want the user generated tag that triggered the release to point to this commit.  
The solution was to move the tag to the commit created by `release:prepare`.  Most git purists would say moving tags ia a [horrible idea](https://stackoverflow.com/a/25849917/7194004).  
I agree for the most part, but the tag created by `release:prepare` is temporary, kind of just a placeholder.  
Plus, Gitlab preserves user entered tag data of the annotated tag, including release notes after the move.

### Release Traceability
Having traceability where the release tag contains the source code, and release notes in one system is glorious!  This is much better than using a separate issue tracking system to track issues and release notes.
For this reason the tag move seems justified.  
